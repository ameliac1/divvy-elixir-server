defmodule DivvyTransactionsWeb.MethodController do
  use DivvyTransactionsWeb, :controller

  alias DivvyTransactions.Transactions
  alias DivvyTransactions.Transactions.Method

  action_fallback DivvyTransactionsWeb.FallbackController

  def index(conn, _params) do
    methods = Transactions.list_methods()
    render(conn, "index.json", methods: methods)
  end

  def create(conn, %{"method" => method_params}) do
    with {:ok, %Method{} = method} <- Transactions.create_method(method_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.method_path(conn, :show, method))
      |> render("show.json", method: method)
    end
  end

  def show(conn, %{"id" => id}) do
    method = Transactions.get_method!(id)
    render(conn, "show.json", method: method)
  end

  def update(conn, %{"id" => id, "method" => method_params}) do
    method = Transactions.get_method!(id)

    with {:ok, %Method{} = method} <- Transactions.update_method(method, method_params) do
      render(conn, "show.json", method: method)
    end
  end

  def delete(conn, %{"id" => id}) do
    method = Transactions.get_method!(id)

    with {:ok, %Method{}} <- Transactions.delete_method(method) do
      send_resp(conn, :no_content, "")
    end
  end
end
