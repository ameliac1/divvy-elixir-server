defmodule DivvyTransactionsWeb.TransactionController do
	use DivvyTransactionsWeb, :controller

	alias DivvyTransactions.Transactions
	alias DivvyTransactions.Transactions.Transaction

	plug :authorize_transaction when action in [:edit, :update, :delete]

	defp authorize_transaction(conn, _) do
		transaction = Transactions.get_transaction!(conn.params["id"])
		current_user_id = get_session(conn, :current_user_id)
		if current_user_id == transaction.user_id do
			assign(conn, :transaction, transaction)
		else
			conn
			|> put_flash(:error, "You can't modify that page")
			|> halt()
		end
	end

	action_fallback DivvyTransactionsWeb.FallbackController

	def index(conn, _params) do
		transactions = Transactions.list_transactions()
		render(conn, "index.json", transactions: transactions)
	end

	def create(conn, %{"transaction" => transaction_params}) do
		with {:ok, %Transaction{} = transaction} <- Transactions.create_transaction(transaction_params) do
			conn
			|> put_status(:created)
			|> render("simple_transaction.json", transaction: transaction)
		end
	end

	def create_bulk(conn, %{"transactions" => transaction_params}) do
		case Transactions.create_bulk_transactions(transaction_params) do
			{:ok, transactions} ->
				conn
				|> put_status(:created)
				|> render("simple_transactions.json", transactions: Map.values(transactions))
			{:error, :log, _failed_value, _changes_successful} ->
				conn
				|> put_status(:unprocessable_entity)
				|> render(:"500")
		end
	end

	def show(conn, %{"id" => id}) do
		transaction = Transactions.get_transaction!(id)
		render(conn, "show.json", transaction: transaction)
	end

	def update(conn, %{"transaction" => transaction_params}) do
		case Transactions.update_transaction(conn.assigns.transaction, transaction_params) do
			{:ok, %Transaction{} = transaction} ->
				conn
				|> render(conn, "show.json", transaction: transaction)
			{:error, _} ->
				conn
				|> put_status(:unprocessable_entity)
				|> render(:"500")
		end
	end

	def delete(conn, _) do
		with {:ok, %Transaction{}} <- Transactions.delete_transaction(conn.assigns.transaction) do
			render(conn, "id.json", id: conn.assigns.transaction.id)
		end
	end

	def user_transactions(conn, %{"id" => id}) do
		transactions = Transactions.list_user_transactions(id)
		render(conn, "index.json", transactions: transactions)
	end
end
