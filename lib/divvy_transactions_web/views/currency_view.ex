defmodule DivvyTransactionsWeb.CurrencyView do
  use DivvyTransactionsWeb, :view
  alias DivvyTransactionsWeb.CurrencyView

  def render("index.json", %{currencies: currencies}) do
    %{currencies: render_many(currencies, CurrencyView, "currency.json")}
  end

  def render("show.json", %{currency: currency}) do
    %{currencies: render_one(currency, CurrencyView, "currency.json")}
  end

  def render("currency.json", %{currency: currency}) do
    %{id: currency.id,
      code: currency.code,
      name: currency.name,
      unicode: currency.unicode}
  end
end
