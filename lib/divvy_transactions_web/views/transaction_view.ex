defmodule DivvyTransactionsWeb.TransactionView do
	use DivvyTransactionsWeb, :view
	alias DivvyTransactionsWeb.TransactionView
	alias DivvyTransactionsWeb.CurrencyView
	alias DivvyTransactionsWeb.CategoryView
	alias DivvyTransactionsWeb.MethodView
	alias DivvyTransactionsWeb.UserView

	def render("index.json", %{transactions: transactions}) do
		%{transactions: render_many(transactions, TransactionView, "transaction.json")}
	end

	def render("simple_transactions.json", %{transactions: transactions}) do
		%{transactions: render_many(transactions, TransactionView, "simple_transaction.json")}
	end

	def render("id.json", %{id: id}) do
		%{id: id}
	end

	def render("show.json", %{transaction: transaction}) do
		%{transactions: render_one(transaction, TransactionView, "transaction.json")}
	end

	def render("transaction.json", %{transaction: transaction}) do
		%{id: transaction.id,
		date: transaction.date,
		title: transaction.title,
		note: transaction.note,
		majorAmount: transaction.major_amount,
		minorAmount: transaction.minor_amount,
		currency: render_one(transaction.currency, CurrencyView, "currency.json"),
		category: render_one(transaction.category, CategoryView, "category.json"),
		method: render_one(transaction.method, MethodView, "method.json"),
		user: render_one(transaction.user, UserView, "public_user.json")}
	end

	def render("simple_transaction.json", %{transaction: transaction}) do
		%{id: transaction.id,
		date: transaction.date,
		title: transaction.title,
		note: transaction.note,
		majorAmount: transaction.major_amount,
		minorAmount: transaction.minor_amount,
		currency_id: transaction.currency_id,
		category_id: transaction.category_id,
		method_id: transaction.method_id,
		user_id: transaction.user_id,}
	end
end
