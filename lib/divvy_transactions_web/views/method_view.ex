defmodule DivvyTransactionsWeb.MethodView do
  use DivvyTransactionsWeb, :view
  alias DivvyTransactionsWeb.MethodView

  def render("index.json", %{methods: methods}) do
    %{methods: render_many(methods, MethodView, "method.json")}
  end

  def render("show.json", %{method: method}) do
    %{methods: render_one(method, MethodView, "method.json")}
  end

  def render("method.json", %{method: method}) do
    %{id: method.id,
      name: method.name}
  end
end
