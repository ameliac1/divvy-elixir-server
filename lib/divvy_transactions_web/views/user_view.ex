defmodule DivvyTransactionsWeb.UserView do
  use DivvyTransactionsWeb, :view
  alias DivvyTransactionsWeb.UserView

  def render("index.json", %{users: users}) do
    %{users: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{user: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id, email: user.email, firstName: user.first_name, lastName: user.last_name}
  end

  def render("public_user.json", %{user: user}) do
  	%{id: user.id, firstName: user.first_name, lastName: user.last_name}
  end

  def render("sign_in.json", %{user: user}) do
    %{
      user: %{
        id: user.id,
        email: user.email,
		firstName: user.first_name,
		lastName: user.last_name,
      }
    }
  end
end
