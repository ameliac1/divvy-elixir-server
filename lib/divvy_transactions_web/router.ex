defmodule DivvyTransactionsWeb.Router do
	use DivvyTransactionsWeb, :router

	pipeline :api do
		plug :accepts, ["json"]
		plug :fetch_session
	end

	pipeline :api_auth do
		plug :ensure_authenticated
	end

	scope "/api", DivvyTransactionsWeb do
		pipe_through :api
		post "/users", UserController, :create
		post "/users/sign_in", UserController, :sign_in
		resources "/categories", CategoryController, except: [:new, :edit]
		resources "/methods", MethodController, except: [:new, :edit]
		resources "/currencies", CurrencyController, except: [:new, :edit]
	end

	scope "/api", DivvyTransactionsWeb do
		pipe_through [:api, :api_auth]
		get "/users/current_user", UserController, :current_user
		resources "/users", UserController, except: [:new, :edit, :create]
		post "/transaction", TransactionController, :create
		post "/transactions", TransactionController, :create_bulk
		resources "/transactions", TransactionController, except: [:new, :edit, :create]
		get "/users/:id/transactions", TransactionController, :user_transactions
	end

	# Plug function
	defp ensure_authenticated(conn, _opts) do
		current_user_id = get_session(conn, :current_user_id)

		if current_user_id do
			conn
		else
			conn
			|> put_status(:unauthorized)
			|> render(DivvyTransactionsWeb.ErrorView, "401.json", message: "Unauthenticated user")
			|> halt()
		end
	end
end
