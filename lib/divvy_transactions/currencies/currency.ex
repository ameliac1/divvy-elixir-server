defmodule DivvyTransactions.Currencies.Currency do
	use Ecto.Schema
	import Ecto.Changeset
	alias DivvyTransactions.Transactions.Transaction


	schema "currencies" do
		field :code, :string
		field :name, :string
		field :unicode, :string

		timestamps()
	end

	@doc false
	def changeset(currency, attrs) do
		currency
		|> cast(attrs, [:code, :name, :unicode])
		|> validate_required([:code, :name, :unicode])
	end
end
