defmodule DivvyTransactions.Currencies do
	@moduledoc """
	The Currencies context.
	"""

	import Ecto.Query, warn: false
	alias DivvyTransactions.Repo

	alias DivvyTransactions.Currencies.Currency

	@doc """
	Returns the list of currencies.
	"""
	def list_currencies do
		Repo.all(Currency)
	end

	@doc """
	Gets a single currency.
	Raises `Ecto.NoResultsError` if the Currency does not exist.
	"""
	def get_currency!(id), do: Repo.get!(Currency, id)

	@doc """
	Creates a currency.
	"""
	def create_currency(attrs \\ %{}) do
		%Currency{}
		|> Currency.changeset(attrs)
		|> Repo.insert()
	end

	@doc """
	Updates a currency.
	"""
	def update_currency(%Currency{} = currency, attrs) do
		currency
		|> Currency.changeset(attrs)
		|> Repo.update()
	end

	@doc """
	Deletes a Currency.
	"""
	def delete_currency(%Currency{} = currency) do
		Repo.delete(currency)
	end

	@doc """
	Returns an `%Ecto.Changeset{}` for tracking currency changes.
	"""
	def change_currency(%Currency{} = currency) do
		Currency.changeset(currency, %{})
	end
end
