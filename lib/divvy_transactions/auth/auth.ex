defmodule DivvyTransactions.Auth do
	@moduledoc """
	The Auth context.
	"""

	import Ecto.Query, warn: false
	alias DivvyTransactions.Repo

	alias DivvyTransactions.Auth.User

	def authenticate_user(email, password) do
		query = from(u in User, where: u.email == ^email)
		query |> Repo.one() |> verify_password(password)
	end

	defp verify_password(nil, _) do
		# Perform a dummy check to make user enumeration more difficult
		Bcrypt.no_user_verify()
		{:error, "Wrong email or password"}
	end

	defp verify_password(user, password) do
		if Bcrypt.verify_pass(password, user.password_hash) do
			{:ok, user}
		else
			{:error, "Wrong email or password"}
		end
	end

	@doc """
	Returns the list of users.
	"""
	def list_users do
		User
		|> Repo.all()
	end

	@doc """
	Gets a single user.
	"""
	def get_user!(id) do
		User
		|> Repo.get!(id)
	end

	@doc """
	Creates a user.
	"""
	def create_user(attrs \\ %{}) do
		%User{}
		|> User.changeset(attrs)
		|> Repo.insert()
	end

	@doc """
	Updates a user.
	"""
	def update_user(%User{} = user, attrs) do
		user
		|> User.changeset(attrs)
		|> Repo.update()
	end

	@doc """
	Deletes a User.
	"""
	def delete_user(%User{} = user) do
		Repo.delete(user)
	end

	@doc """
		Returns an `%Ecto.Changeset{}` for tracking user changes.
	"""
	def change_user(%User{} = user) do
		User.changeset(user, %{})
	end
end
