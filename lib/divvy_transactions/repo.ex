defmodule DivvyTransactions.Repo do
  use Ecto.Repo,
    otp_app: :divvy_transactions,
    adapter: Ecto.Adapters.Postgres
end
