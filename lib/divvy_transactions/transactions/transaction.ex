alias DivvyTransactions.Transactions.Category
alias DivvyTransactions.Transactions.Method
alias DivvyTransactions.Currencies.Currency
alias DivvyTransactions.Auth.User

defmodule DivvyTransactions.Transactions.Transaction do
	use Ecto.Schema
	import Ecto.Changeset


	schema "transactions" do
		field :date, :date
		field :major_amount, :integer
		field :minor_amount, :integer
		field :note, :string
		field :title, :string

		belongs_to :user, User
		belongs_to :category, Category
		belongs_to :method, Method
		belongs_to :currency, Currency

		timestamps()
	end

	@doc false
	def changeset(transaction, attrs) do
		transaction
		|> cast(attrs, [:date, :title, :note, :major_amount, :minor_amount, :user_id, :method_id, :category_id, :currency_id])
		|> validate_required([:date, :title, :major_amount, :minor_amount, :user_id, :method_id, :category_id, :currency_id])
	end
end
