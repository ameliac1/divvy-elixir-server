defmodule DivvyTransactions.Transactions do
  @moduledoc """
  The Transactions context.
  """

  import Ecto.Query, warn: false
  alias DivvyTransactions.Repo
  alias DivvyTransactions.Transactions.{Transaction, Method, Category}


  @doc """
  Returns the list of categories.
  """
  def list_categories do
    Repo.all(Category)
  end

  @doc """
  Gets a single category.
  Raises `Ecto.NoResultsError` if the Category does not exist.
  """
  def get_category!(id), do: Repo.get!(Category, id)

  @doc """
  Creates a category.
  """
  def create_category(attrs \\ %{}) do
    %Category{}
    |> Category.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a category.
  """
  def update_category(%Category{} = category, attrs) do
    category
    |> Category.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Category.
  """
  def delete_category(%Category{} = category) do
    Repo.delete(category)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking category changes.
  """
  def change_category(%Category{} = category) do
    Category.changeset(category, %{})
  end

  @doc """
  Returns the list of methods.
  """
  def list_methods do
    Repo.all(Method)
  end

  @doc """
  Gets a single method.
  Raises `Ecto.NoResultsError` if the Method does not exist.
  """
  def get_method!(id), do: Repo.get!(Method, id)

  @doc """
  Creates a method.
  """
  def create_method(attrs \\ %{}) do
    %Method{}
    |> Method.changeset(attrs)
    |> Repo.insert()
  end

	@doc """
	Updates a method.
	"""
	def update_method(%Method{} = method, attrs) do
		method
		|> Method.changeset(attrs)
		|> Repo.update()
	end

	@doc """
	Deletes a Method.
	"""
	def delete_method(%Method{} = method) do
		Repo.delete(method)
	end

	@doc """
	Returns an `%Ecto.Changeset{}` for tracking method changes.
	"""
	def change_method(%Method{} = method) do
		Method.changeset(method, %{})
	end

	@doc """
	Returns the list of transactions.
	"""
	def list_transactions do
		Transaction
			|> Repo.all()
			|> Repo.preload([:user, :category, :method, :currency])
	end

	@doc """
	Returns the list of transactions.
	"""
	def list_user_transactions(id) do
		query = from(t in Transaction, where: t.user_id == ^id, order_by: t.date)
		query
			|> Repo.all()
			|> Repo.preload([:user, :category, :method, :currency])
	end

	@doc """
	Gets a single transaction.
	Raises `Ecto.NoResultsError` if the Transaction does not exist.
	"""
	def get_transaction!(id) do
		Transaction
			|> Repo.get!(id)
			|> Repo.preload([:user, :category, :method, :currency])
	end

	@doc """
	Creates a transaction.
	"""
	def create_transaction(attrs \\ %{}) do
		%Transaction{}
		|> Transaction.changeset(attrs)
		|> Repo.insert()
	end

	@doc """
	Creates transactions from a list.
	"""
	def create_bulk_transactions(transactions \\ %{}) do
		transactions
			|> Enum.map(fn attrs ->
				# Create changesets.
				%Transaction{} |> Transaction.changeset(attrs)
			end)
			|> Enum.reduce(Ecto.Multi.new, fn(cset, multi) -> Ecto.Multi.insert(multi, Ecto.UUID.generate, cset) end)
			|> Repo.transaction()
	end

	@doc """
	Updates a transaction.
	"""
	def update_transaction(%Transaction{} = transaction, attrs) do
		transaction
			|> Transaction.changeset(attrs)
			|> Repo.update()
	end

	@doc """
	Deletes a Transaction.
	"""
	def delete_transaction(%Transaction{} = transaction) do
		Repo.delete(transaction)
	end

	@doc """
	Returns an `%Ecto.Changeset{}` for tracking transaction changes.
	"""
	def change_transaction(%Transaction{} = transaction) do
		Transaction.changeset(transaction, %{})
	end
end
