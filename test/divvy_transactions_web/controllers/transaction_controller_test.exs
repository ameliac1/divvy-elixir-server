defmodule DivvyTransactionsWeb.TransactionControllerTest do
  use DivvyTransactionsWeb.ConnCase

  alias DivvyTransactions.Transactions
  alias DivvyTransactions.Transactions.Transaction

  @create_attrs %{
    currencyCode: "some currencyCode",
    date: ~D[2010-04-17],
    dateSubmitted: ~D[2010-04-17],
    m: "some m",
    majorAmount: 42,
    minorAmount: 42,
    note: "some note",
    title: "some title"
  }
  @update_attrs %{
    currencyCode: "some updated currencyCode",
    date: ~D[2011-05-18],
    dateSubmitted: ~D[2011-05-18],
    m: "some updated m",
    majorAmount: 43,
    minorAmount: 43,
    note: "some updated note",
    title: "some updated title"
  }
  @invalid_attrs %{currencyCode: nil, date: nil, dateSubmitted: nil, m: nil, majorAmount: nil, minorAmount: nil, note: nil, title: nil}

  def fixture(:transaction) do
    {:ok, transaction} = Transactions.create_transaction(@create_attrs)
    transaction
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all transactions", %{conn: conn} do
      conn = get(conn, Routes.transaction_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create transaction" do
    test "renders transaction when data is valid", %{conn: conn} do
      conn = post(conn, Routes.transaction_path(conn, :create), transaction: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.transaction_path(conn, :show, id))

      assert %{
               "id" => id,
               "currencyCode" => "some currencyCode",
               "date" => "2010-04-17",
               "dateSubmitted" => "2010-04-17",
               "m" => "some m",
               "majorAmount" => 42,
               "minorAmount" => 42,
               "note" => "some note",
               "title" => "some title"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.transaction_path(conn, :create), transaction: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update transaction" do
    setup [:create_transaction]

    test "renders transaction when data is valid", %{conn: conn, transaction: %Transaction{id: id} = transaction} do
      conn = put(conn, Routes.transaction_path(conn, :update, transaction), transaction: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.transaction_path(conn, :show, id))

      assert %{
               "id" => id,
               "currencyCode" => "some updated currencyCode",
               "date" => "2011-05-18",
               "dateSubmitted" => "2011-05-18",
               "m" => "some updated m",
               "majorAmount" => 43,
               "minorAmount" => 43,
               "note" => "some updated note",
               "title" => "some updated title"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, transaction: transaction} do
      conn = put(conn, Routes.transaction_path(conn, :update, transaction), transaction: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete transaction" do
    setup [:create_transaction]

    test "deletes chosen transaction", %{conn: conn, transaction: transaction} do
      conn = delete(conn, Routes.transaction_path(conn, :delete, transaction))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.transaction_path(conn, :show, transaction))
      end
    end
  end

  defp create_transaction(_) do
    transaction = fixture(:transaction)
    {:ok, transaction: transaction}
  end
end
