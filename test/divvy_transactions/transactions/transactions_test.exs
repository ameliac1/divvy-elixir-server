defmodule DivvyTransactions.TransactionsTest do
  use DivvyTransactions.DataCase

  alias DivvyTransactions.Transactions

  describe "categories" do
    alias DivvyTransactions.Transactions.Category

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def category_fixture(attrs \\ %{}) do
      {:ok, category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Transactions.create_category()

      category
    end

    test "list_categories/0 returns all categories" do
      category = category_fixture()
      assert Transactions.list_categories() == [category]
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert Transactions.get_category!(category.id) == category
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = Transactions.create_category(@valid_attrs)
      assert category.name == "some name"
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Transactions.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, %Category{} = category} = Transactions.update_category(category, @update_attrs)
      assert category.name == "some updated name"
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = Transactions.update_category(category, @invalid_attrs)
      assert category == Transactions.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      assert {:ok, %Category{}} = Transactions.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Transactions.get_category!(category.id) end
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = Transactions.change_category(category)
    end
  end

  describe "methods" do
    alias DivvyTransactions.Transactions.Method

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def method_fixture(attrs \\ %{}) do
      {:ok, method} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Transactions.create_method()

      method
    end

    test "list_methods/0 returns all methods" do
      method = method_fixture()
      assert Transactions.list_methods() == [method]
    end

    test "get_method!/1 returns the method with given id" do
      method = method_fixture()
      assert Transactions.get_method!(method.id) == method
    end

    test "create_method/1 with valid data creates a method" do
      assert {:ok, %Method{} = method} = Transactions.create_method(@valid_attrs)
      assert method.name == "some name"
    end

    test "create_method/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Transactions.create_method(@invalid_attrs)
    end

    test "update_method/2 with valid data updates the method" do
      method = method_fixture()
      assert {:ok, %Method{} = method} = Transactions.update_method(method, @update_attrs)
      assert method.name == "some updated name"
    end

    test "update_method/2 with invalid data returns error changeset" do
      method = method_fixture()
      assert {:error, %Ecto.Changeset{}} = Transactions.update_method(method, @invalid_attrs)
      assert method == Transactions.get_method!(method.id)
    end

    test "delete_method/1 deletes the method" do
      method = method_fixture()
      assert {:ok, %Method{}} = Transactions.delete_method(method)
      assert_raise Ecto.NoResultsError, fn -> Transactions.get_method!(method.id) end
    end

    test "change_method/1 returns a method changeset" do
      method = method_fixture()
      assert %Ecto.Changeset{} = Transactions.change_method(method)
    end
  end

  describe "transactions" do
    alias DivvyTransactions.Transactions.Transaction

    @valid_attrs %{user_id: 1, method_id: 1, category_id: 1, currencyCode: "some currencyCode", date: ~D[2010-04-17], dateSubmitted: ~D[2010-04-17], majorAmount: 42, minorAmount: 42, note: "some note", title: "some title"}
    @update_attrs %{user_id: 2, method_id: 2, category_id: 2, currencyCode: "some updated currencyCode", date: ~D[2011-05-18], dateSubmitted: ~D[2011-05-18], majorAmount: 43, minorAmount: 43, note: "some updated note", title: "some updated title"}
    @invalid_attrs %{user_id: 1, method_id: nil, category_id: nil, currencyCode: nil, date: nil, dateSubmitted: nil, majorAmount: nil, minorAmount: nil, note: nil, title: nil}

    def transaction_fixture(attrs \\ %{}) do
      {:ok, transaction} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Transactions.create_transaction()

      transaction
    end

    test "list_transactions/0 returns all transactions" do
      transaction = transaction_fixture()
      assert Transactions.list_transactions() == [transaction]
    end

    test "get_transaction!/1 returns the transaction with given id" do
      transaction = transaction_fixture()
      assert Transactions.get_transaction!(transaction.id) == transaction
    end

    test "create_transaction/1 with valid data creates a transaction" do
      assert {:ok, %Transaction{} = transaction} = Transactions.create_transaction(@valid_attrs)
      assert transaction.currencyCode == "some currencyCode"
      assert transaction.date == ~D[2010-04-17]
      assert transaction.dateSubmitted == ~D[2010-04-17]
      assert transaction.majorAmount == 42
      assert transaction.minorAmount == 42
      assert transaction.note == "some note"
      assert transaction.title == "some title"
    end

    test "create_transaction/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Transactions.create_transaction(@invalid_attrs)
    end

    test "update_transaction/2 with valid data updates the transaction" do
      transaction = transaction_fixture()
      assert {:ok, %Transaction{} = transaction} = Transactions.update_transaction(transaction, @update_attrs)
      assert transaction.currencyCode == "some updated currencyCode"
      assert transaction.date == ~D[2011-05-18]
      assert transaction.dateSubmitted == ~D[2011-05-18]
      assert transaction.majorAmount == 43
      assert transaction.minorAmount == 43
      assert transaction.note == "some updated note"
      assert transaction.title == "some updated title"
    end

    test "update_transaction/2 with invalid data returns error changeset" do
      transaction = transaction_fixture()
      assert {:error, %Ecto.Changeset{}} = Transactions.update_transaction(transaction, @invalid_attrs)
      assert transaction == Transactions.get_transaction!(transaction.id)
    end

    test "delete_transaction/1 deletes the transaction" do
      transaction = transaction_fixture()
      assert {:ok, %Transaction{}} = Transactions.delete_transaction(transaction)
      assert_raise Ecto.NoResultsError, fn -> Transactions.get_transaction!(transaction.id) end
    end

    test "change_transaction/1 returns a transaction changeset" do
      transaction = transaction_fixture()
      assert %Ecto.Changeset{} = Transactions.change_transaction(transaction)
    end
  end
end
