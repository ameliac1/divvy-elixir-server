# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :divvy_transactions,
  ecto_repos: [DivvyTransactions.Repo]

# Configures the endpoint
config :divvy_transactions, DivvyTransactionsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "C5CMpUg38Oyvb6YR7wyInFpHTRiddm/ngWeOJFexi+FXWgWPvW/w1sOecWwhXLtp",
  render_errors: [view: DivvyTransactionsWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: DivvyTransactions.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
