# DivvyTransactions

# NOTES AND DISCLAIMERS:

1) This is my first application written in Elixir, and first time writing anything at all in Elixir, so I'm positive there are multiple ways I can improve on this implementation. I would love any feedback, it has been
fun to experiment with a new language and framework, and I would love to learn more and improve.
2) Tests are not fully fleshed out. I did a few of them, but as I got deeper into the project I stopped
updating and fixing them. In a real production application, I would prefer to do them to cover all bases.
3) There is initial data that needs to be inserted into the database. I assume the seeds file will run on ecto.setup, but if not then they need to be run. (mix run priv/repo/seeds.exs)
4) I wouldn't normally leave db login information in a public bitbucket repo, but since it's just for my local I'm going to go ahead and assume it doesn't matter in this case.


To start the server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`
