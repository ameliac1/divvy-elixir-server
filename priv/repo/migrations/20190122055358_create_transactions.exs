defmodule DivvyTransactions.Repo.Migrations.CreateTransactions do
	use Ecto.Migration

	def change do
		create table(:transactions) do
			add :date, :date, null: false
			add :title, :string, null: false
			add :note, :string
			add :major_amount, :integer, null: false
			add :minor_amount, :integer, null: false
			add :currency_id, references(:currencies, on_delete: :nothing), null: false
			add :user_id, references(:users, on_delete: :delete_all), null: false
			add :category_id, references(:categories, on_delete: :nothing), null: false
			add :method_id, references(:methods, on_delete: :nothing), null: false

			timestamps()
		end

		create index(:transactions, [:user_id])
		create index(:transactions, [:category_id])
		create index(:transactions, [:method_id])
		create index(:transactions, [:currency_id])
	end

end
