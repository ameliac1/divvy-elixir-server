defmodule DivvyTransactions.Repo.Migrations.CreateMethods do
	use Ecto.Migration
	def up do
		create table(:methods) do
			add :name, :string
			timestamps()
		end

		create unique_index(:methods, [:name])
	end

	def down do

	end
end
