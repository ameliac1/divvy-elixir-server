defmodule DivvyTransactions.Repo.Migrations.CreateCurrencies do
	use Ecto.Migration

	def change do
		create table(:currencies) do
			add :code, :string
			add :name, :string
			add :unicode, :string

			timestamps()
		end

		create unique_index(:currencies, [:code])
	end

	def down do

	end
end
