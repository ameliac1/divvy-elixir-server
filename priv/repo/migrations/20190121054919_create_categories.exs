defmodule DivvyTransactions.Repo.Migrations.CreateCategories do
	use Ecto.Migration

	def up do
		create table(:categories) do
			add :name, :string
			timestamps()
		end

		create unique_index(:categories, [:name])
	end

	def down do
		# drop table
	end
end
