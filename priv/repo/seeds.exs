alias DivvyTransactions.Transactions
alias DivvyTransactions.Currencies

# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs

Transactions.create_category(%{name: "Gifts"})
Transactions.create_category(%{name: "Food"})
Transactions.create_category(%{name: "Travel"})
Transactions.create_category(%{name: "Entertainment"})
Transactions.create_category(%{name: "Utilities"})
Transactions.create_category(%{name: "Housing"})
Transactions.create_category(%{name: "Income"})
Transactions.create_category(%{name: "Education"})
Transactions.create_category(%{name: "Health Care"})
Transactions.create_category(%{name: "Clothing"})
Transactions.create_category(%{name: "Personal Care"})
Transactions.create_category(%{name: "Miscellaneous"})

Transactions.create_method(%{name: "Credit"})
Transactions.create_method(%{name: "Debit"})
Transactions.create_method(%{name: "Check"})
Transactions.create_method(%{name: "Cash"})
Transactions.create_method(%{name: "Direct Deposit"})
Transactions.create_method(%{name: "Other"})

Currencies.create_currency(%{code: "AUD", name: "Australian Dollar", unicode: "&#x24;"})
Currencies.create_currency(%{code: "CAD", name: "Canadian Dollar", unicode: "&#x24;"})
Currencies.create_currency(%{code: "CNY", name: "China Yuan Renminbi", unicode: "&#xa5;"})
Currencies.create_currency(%{code: "EUR", name: "Euro Member Countries", unicode: "&#x20ac;"})
Currencies.create_currency(%{code: "HUF", name: "Hungarian Forint", unicode: "&#x46;&#x74;"})
Currencies.create_currency(%{code: "JPY", name: "Japan Yen", unicode: "&#xa5;"})
Currencies.create_currency(%{code: "MXN", name: "Mexico Peso", unicode: "&#x24;"})
Currencies.create_currency(%{code: "SEK", name: "Sweden Krona", unicode: "&#x6b;&#x72;"})
Currencies.create_currency(%{code: "GBP", name: "United Kingdom Pound", unicode: "&#xa3;"})
Currencies.create_currency(%{code: "USD", name: "United States Dollar", unicode: "&#x24;"})
Currencies.create_currency(%{code: "VND", name: "Viet Nam Dong", unicode: "&#x20ab;"})
